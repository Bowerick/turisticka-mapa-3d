package com.example.robo.turistickamapasr3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.opengl.GLUtils;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

import javax.microedition.khronos.opengles.GL10;


/**
 * Created by Robo on 12.04.2017.
 */
public class Area {

    private static final String TAG = "Area";
    private static final int ZOOM_12 = 12;
    private static final int SIDE = 100;
    private static final float JEDEN_STUPEN_AKTUAL_DLZKY_V_KM = 73f;
    private static final float JEDEN_STUPEN_AKTUAL_SIRKY_V_KM = 111.0f;
    private Location _location;
    private FloatBuffer _textureBuffer;

    private int[] texHandles = new int[]{1};
    private Bitmap _bitmap;

    private Context _context;
    private Bitmap _noTileBitmap;

    private FloatBuffer _vertexBuffer;
    private int _verticesLength;
    private int _row;
    private int _col;
    private short[] _ele;

    public Area(Context context, Location location, FloatBuffer textureBuffer, int i, int j, Bitmap bitmap) {
        this._context = context;
        this._location = location;
        this._textureBuffer = textureBuffer;
        this._row = i;
        this._col = j;
        this._noTileBitmap = bitmap;
        loadVertices();

    }

    public void draw(GL10 gl) {
        gl.glFrontFace(GL10.GL_CCW);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, texHandles[0]);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, _vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, _textureBuffer);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glDrawArrays(GL10.GL_TRIANGLES, 0, _verticesLength);
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    }

    public void loadVertices() {
        float[] _vertices = new float[3 * 3 * 2 * (SIDE) * (SIDE)];

        float[] arrayX = new float[SIDE+1];
        for(int i=0;i<arrayX.length;i++) {
            arrayX[i] = 5*(((float)i)/((float)SIDE))*(JEDEN_STUPEN_AKTUAL_DLZKY_V_KM / JEDEN_STUPEN_AKTUAL_SIRKY_V_KM);
        }

        float[] arrayZ = new float[SIDE+1];
        for(int i=0;i<arrayZ.length;i++) {
            arrayZ[i] = /*-1f*/ + 5*(((float)i)/((float)SIDE));
        }

        int[] tiles = getTileNumber(_location.getLatitude(),_location.getLongitude(),ZOOM_12);
        tiles[0] += _col;
        tiles[1] += _row;
        int size;
        _ele = new short[(SIDE+1)*(SIDE+1)];
        try {
            size = this._context.getAssets().open("tiles/12/" +tiles[0]+ "/" +tiles[1]+ ".bin").available();
            byte[] bytes = new byte[size];
            this._context.getAssets().open("tiles/12/" +tiles[0]+ "/" +tiles[1]+ ".bin").read(bytes);
            ShortBuffer fb = ByteBuffer.wrap(bytes).asShortBuffer();
            fb.get(_ele);

        } catch (IOException e) {
            Log.e(TAG,""+e.toString());
            e.printStackTrace();
            Arrays.fill(_ele,(short)0);
        }
        float width =(float) (tile2lat(tiles[1],ZOOM_12)-tile2lat(tiles[1]+1,ZOOM_12));

        for (int i = 0; i < (SIDE); i++) {
            for (int j = 0; j < (SIDE); j++) {
                _vertices[(i*(SIDE) + j) * 18 + 0] = arrayX[j];
                _vertices[(i*(SIDE) + j) * 18 + 1] = 5* _ele[i*(SIDE+1) + j]/(width*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000f);
                _vertices[(i*(SIDE) + j) * 18 + 2] = arrayZ[i];
                //----------------------------
                _vertices[(i*(SIDE) + j) * 18 + 3] = arrayX[j+1];
                _vertices[(i*(SIDE) + j) * 18 + 4] = 5* _ele[i*(SIDE+1) + j + 1]/(width*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000f);
                _vertices[(i*(SIDE) + j) * 18 + 5] = arrayZ[i];
                //------------------ ----------
                _vertices[(i*(SIDE) + j) * 18 + 6] = arrayX[j];
                _vertices[(i*(SIDE) + j) * 18 + 7] = 5* _ele[(i+1)*(SIDE+1) + j]/(width*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000f);
                _vertices[(i*(SIDE) + j) * 18 + 8] = arrayZ[i+1];
                //-----------------------------------------------------------------------------------
                _vertices[(i*(SIDE) + j) * 18 + 9] = arrayX[j];
                _vertices[(i*(SIDE) + j) * 18 + 10] = 5* _ele[(i+1)*(SIDE+1) + j]/(width*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000f);
                _vertices[(i*(SIDE) + j) * 18 + 11] = arrayZ[i+1];
                //----------------------------
                _vertices[(i*(SIDE) + j) * 18 + 12] = arrayX[j+1];
                _vertices[(i*(SIDE) + j) * 18 + 13] = 5* _ele[i*(SIDE+1) + j + 1]/(width*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000f);
                _vertices[(i*(SIDE) + j) * 18 + 14] = arrayZ[i];
                //----------------------------
                _vertices[(i*(SIDE) + j) * 18 + 15] = arrayX[j+1];
                _vertices[(i*(SIDE) + j) * 18 + 16] = 5* _ele[(i+1)*(SIDE+1) + j +1]/(width*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000f);
                _vertices[(i*(SIDE) + j) * 18 + 17] = arrayZ[i+1];
            }
        }

        ByteBuffer byteBuf = ByteBuffer.allocateDirect(_vertices.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        _vertexBuffer = byteBuf.asFloatBuffer();
        _vertexBuffer.put(_vertices);
        _vertexBuffer.position(0);
        this._verticesLength = _vertices.length/3;
    }

    public void loadGLTexture(GL10 gl) {
        gl.glGenTextures(1, texHandles, 0);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, texHandles[0]);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        if(_bitmap == null) {
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, _noTileBitmap, 0);
        }
        else {
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, _bitmap, 0);
            _bitmap.recycle();
        }
    }

    public void setBitmap(Bitmap bitmap) {
        this._bitmap = bitmap.copy(bitmap.getConfig(),true);
    }


    private static int[] getTileNumber(final double lat, final double lon, final int zoom) {
        int xtile = (int)Math.floor( (lon + 180) / 360 * (1<<zoom) ) ;
        int ytile = (int)Math.floor( (1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1<<zoom) ) ;
        if (xtile < 0)
            xtile=0;
        if (xtile >= (1<<zoom))
            xtile=((1<<zoom)-1);
        if (ytile < 0)
            ytile=0;
        if (ytile >= (1<<zoom))
            ytile=((1<<zoom)-1);
        return new int[]{xtile,ytile};
    }

    private static double tile2lat(int y, int z) {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }

    private static double tile2lon(int x, int z) {
        return x / Math.pow(2.0, z) * 360.0 - 180;
    }


    public short getElevation(Location loc) {
        double maxLat = tile2lat(getTileNumber(loc.getLatitude(),loc.getLongitude(),ZOOM_12)[1]/*-2*/,ZOOM_12);
        double minLat = tile2lat(getTileNumber(loc.getLatitude(),loc.getLongitude(),ZOOM_12)[1]+1/*+3*/,ZOOM_12);
        double maxLon = tile2lon(getTileNumber(loc.getLatitude(),loc.getLongitude(),ZOOM_12)[0]+1/*+3*/,ZOOM_12);
        double minLon = tile2lon(getTileNumber(loc.getLatitude(),loc.getLongitude(),ZOOM_12)[0]/*-2*/,ZOOM_12);
        int lat = (int)(((maxLat - loc.getLatitude())/(maxLat-minLat))*100);
        int lon = (int)(((loc.getLongitude()-minLon)/(maxLon-minLon))*100);
        return _ele[(lat*(SIDE)) +lon];
    }
}