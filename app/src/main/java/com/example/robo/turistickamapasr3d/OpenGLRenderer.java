package com.example.robo.turistickamapasr3d;
/**
 * Created by Robo on 16.06.2017.
 */

import android.location.Location;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Robo on 12.04.2017.
 */

public class OpenGLRenderer implements GLSurfaceView.Renderer {

    private static final int ZOOM_12 = 12;
    private static final float JEDEN_STUPEN_AKTUAL_DLZKY_V_KM = 73.0f;
    private static final float JEDEN_STUPEN_AKTUAL_SIRKY_V_KM = 111.0f;
    private Area[] area;

    private float _translateX;
    private float _translateY;
    private float _translateZ;
    private Location location;
    private int _tileLonCenter;
    private int _tileLatCenter;
    private boolean _changed;

    public OpenGLRenderer(Location loc, Area[] areas) {

        this.location = loc;

        this.location = new Location("My");
        this.getLocation().setLatitude(49.404);
        this.getLocation().setLongitude(19.47);

        area = areas;
        //X
        int currentTileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];
        double maxLat = tile2lat(currentTileLatCenter,ZOOM_12);
        double minLat = tile2lat(currentTileLatCenter+1,ZOOM_12);
        this._translateZ =5*(-1+(float)(((this.getLocation().getLatitude()-minLat)/((maxLat-minLat)))));
        //Y
        this._translateY = (float)((-1*(5*area[4].getElevation(getLocation())))/((maxLat-minLat)*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000))-1.0f;
        //Z
        double width = (JEDEN_STUPEN_AKTUAL_DLZKY_V_KM / JEDEN_STUPEN_AKTUAL_SIRKY_V_KM);
        int currentTileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];
        double maxLon = tile2lon(currentTileLonCenter+1,ZOOM_12);
        double minLon = tile2lon(currentTileLonCenter,ZOOM_12);
        this._translateX =5*((float) ((-1*(this.getLocation().getLongitude() -minLon)/((maxLon-minLon)))*width));

        this._tileLonCenter = -1;
        this._tileLatCenter = -1;
        this._changed = false;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl.glEnable(GL10.GL_TEXTURE_2D);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if(_changed) {
            for(int i=-1;i<=1;i++) {
                for (int j = -1; j <= 1; j++) {
                    area[(i + 1) * 3 + (j + 1)].loadGLTexture(gl);
                }
            }
            this._changed = false;
        }

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();

        gl.glTranslatef(_translateX, _translateY, _translateZ);

        float posun = 5*(JEDEN_STUPEN_AKTUAL_DLZKY_V_KM/JEDEN_STUPEN_AKTUAL_SIRKY_V_KM);


        boolean first=true;
        for(int i=-1;i<=1;i++) {
            for (int j = -1; j <= 1; j++) {
                if(j==-1) {
                    if(first) {
                        //area[(i + 1) * 3 + (j + 1)].draw(gl);
                        gl.glTranslatef(-posun, 0, -5.0f);
                        first = false;
                    } else {
                        gl.glTranslatef(-2*posun, 0, 5.0f);
                    }
                } else {
                    gl.glTranslatef(posun, 0, 0);
                }
                area[(i + 1) * 3 + (j + 1)].draw(gl);
            }
        }
        gl.glLoadIdentity();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        GLU.gluPerspective(gl, 45.0f, (float)width / (float)height, 0.1f, 100.0f);//45.0f
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public int setPosuvZ(float posuvZ) {
        double maxLat = tile2lat(getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1]/*-2*/,ZOOM_12);
        double minLat = tile2lat(getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1]+1/*+3*/,ZOOM_12);
        double shift = (posuvZ*(maxLat-minLat));
        this.getLocation().setLatitude(this.getLocation().getLatitude() + shift);
        int currentTileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];

        maxLat = tile2lat(currentTileLatCenter,ZOOM_12);
        minLat = tile2lat(currentTileLatCenter+1,ZOOM_12);
        this._translateZ =5*(-1+(float)(((this.getLocation().getLatitude()-minLat)/((maxLat-minLat)))));
        this._translateY = (float)((-1*(5*area[4].getElevation(getLocation())))/((maxLat-minLat)*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000))-1.0f;

        if(_tileLatCenter != -1 && _tileLatCenter != currentTileLatCenter) {
            if(_tileLatCenter < currentTileLatCenter) {
                _tileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];
                return -1;
            }
            if(_tileLatCenter > currentTileLatCenter) {
                _tileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];
                return 1;

            }

        }
        _tileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];
        return 0;
    }

    public int setPosuvX(float posuvX) {
        double width = (JEDEN_STUPEN_AKTUAL_DLZKY_V_KM / JEDEN_STUPEN_AKTUAL_SIRKY_V_KM);
        double maxLon = tile2lon(getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0]+1/*+3*/,ZOOM_12);
        double minLon = tile2lon(getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0]/*-2*/,ZOOM_12);
        double shift = (-posuvX*(maxLon-minLon));
        this.getLocation().setLongitude(this.getLocation().getLongitude() + shift);
        int currentTileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];

        maxLon = tile2lon(currentTileLonCenter+1,ZOOM_12);
        minLon = tile2lon(currentTileLonCenter,ZOOM_12);
        this._translateX =5*((float) ((-1*(this.getLocation().getLongitude() -minLon)/((maxLon-minLon)))*width));
        int currentTileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];
        double maxLat = tile2lat(currentTileLatCenter,ZOOM_12);
        double minLat = tile2lat(currentTileLatCenter+1,ZOOM_12);
        this._translateY = (float)((-1*(5*area[4].getElevation(getLocation())))/((maxLat-minLat)*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000))-1.0f;

        if(_tileLonCenter != -1 && _tileLonCenter != currentTileLonCenter) {
            if(_tileLonCenter > currentTileLonCenter) {
                _tileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];
                return 1;
            }
            if(_tileLonCenter < currentTileLonCenter) {
                _tileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];
                return -1;
            }
        }
        _tileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];
        return 0;
    }

    private static int[] getTileNumber(final double lat, final double lon, final int zoom) {
        int xtile = (int)Math.floor( (lon + 180) / 360 * (1<<zoom) ) ;
        int ytile = (int)Math.floor( (1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1<<zoom) ) ;
        if (xtile < 0)
            xtile=0;
        if (xtile >= (1<<zoom))
            xtile=((1<<zoom)-1);
        if (ytile < 0)
            ytile=0;
        if (ytile >= (1<<zoom))
            ytile=((1<<zoom)-1);
        return new int[]{xtile,ytile};
    }

    private static double tile2lat(int y, int z) {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }

    private static double tile2lon(int x, int z) {
        return x / Math.pow(2.0, z) * 360.0 - 180;
    }

    public void setAreas(Area[] areas) {
        this.area = areas;
    }

    public void setChange(boolean change) {
        this._changed = change;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location.setLatitude(location.getLatitude());
        this.location.setLongitude(location.getLongitude());

        _tileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];
        _tileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];

        double width = (JEDEN_STUPEN_AKTUAL_DLZKY_V_KM / JEDEN_STUPEN_AKTUAL_SIRKY_V_KM);
        int currentTileLonCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[0];
        double maxLon = tile2lon(currentTileLonCenter+1,ZOOM_12);
        double minLon = tile2lon(currentTileLonCenter,ZOOM_12);
        this._translateX =5*((float) ((-1*(this.getLocation().getLongitude() -minLon)/((maxLon-minLon)))*width));

        int currentTileLatCenter = getTileNumber(this.getLocation().getLatitude(), this.getLocation().getLongitude(),ZOOM_12)[1];
        double maxLat = tile2lat(currentTileLatCenter,ZOOM_12);
        double minLat = tile2lat(currentTileLatCenter+1,ZOOM_12);
        this._translateZ =5*(-1+(float)( ((this.getLocation().getLatitude()-minLat)/((maxLat-minLat)))));

        this._translateY = (float)((-1*(5*area[4].getElevation(getLocation())))/((maxLat-minLat)*JEDEN_STUPEN_AKTUAL_SIRKY_V_KM*1000))-1.0f;

    }
}