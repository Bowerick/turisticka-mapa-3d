package com.example.robo.turistickamapasr3d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    private static final String TAG = "MainActivity";
    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    private static final int SIDE = 100;

    private OpenGLRenderer _renderer;
    private GLSurfaceView _view;
    private FrameLayout _frameLayout;

    private float _startX;
    private float _startY;
    private boolean _isEvent;

    private Area[] _area;
    private Location _location;
    private Location _GPSLocation;
    private BroadcastReceiver _broadcastReceiver;
    private FloatBuffer _textureBuffer;
    private Bitmap _noTileBitmap;
    private Executor _singleThreadExecutor;
    private Context _context;
    private SharedPreferences _settings;
    private SharedPreferences.Editor _editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        if(_broadcastReceiver == null){
            _broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String coord = (String) intent.getExtras().get("coordinates");
                    String[] tokens = coord.split("-");
                    double lon = Double.parseDouble(tokens[0]);
                    double lat = Double.parseDouble(tokens[1]);
                    String provider = tokens[2];
                    _GPSLocation = new Location(provider);
                    _GPSLocation.setLatitude(lat);
                    _GPSLocation.setLongitude(lon);
                    Log.d(TAG,"GPSLocation: " +intent.getExtras().get("coordinates"));
                }
            };
        }
        registerReceiver(_broadcastReceiver,new IntentFilter("Location_update"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
        Intent intent =new Intent(getApplicationContext(),GPSService.class);
        startService(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //_settings = getSharedPreferences(PREFS_NAME, 0);
        //SharedPreferences.Editor edit = _settings.edit();
        //_editor = _settings.edit();
        if(_GPSLocation != null) {
            _editor.putFloat(LATITUDE, (float) _GPSLocation.getLatitude());
            _editor.putFloat(LONGITUDE, (float) _GPSLocation.getLongitude());
            _editor.commit();
        }

        Intent intent =new Intent(getApplicationContext(),GPSService.class);
        stopService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(_broadcastReceiver != null){
            unregisterReceiver(_broadcastReceiver);
        }
    }

    private void init() {
        _context = this;
        _singleThreadExecutor = Executors.newSingleThreadExecutor();
        new ImageLoader2(this).executeOnExecutor(_singleThreadExecutor);
        _area = new Area[9];

        _settings = getSharedPreferences(PREFS_NAME, 0);
        _editor = _settings.edit();
        this._location = new Location("My");
        this._location.setLatitude(_settings.getFloat(LATITUDE,48.73025f));
        this._location.setLongitude(_settings.getFloat(LONGITUDE,19.13695f));

        loadTextureBuffer();

        for(int i=-1;i<=1;i++) {
            for (int j = -1; j <= 1; j++) {
                _area[(i + 1) * 3 + (j + 1)] = new Area(this, _location, _textureBuffer, i, j, _noTileBitmap);
            }
        }
        new ImageLoader(this, _location).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

        _renderer = new OpenGLRenderer(new Location(""), _area);

        _view = new GLSurfaceView(this);
        _view.setRenderer(_renderer);
        _view.setOnTouchListener(this);

        _frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        _frameLayout.addView(_view);

        ImageButton buttonLocation = (ImageButton) findViewById(R.id.imageButton);
        buttonLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager lm = (LocationManager) getSystemService(_context.LOCATION_SERVICE);
                if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.noGPS), Toast.LENGTH_LONG).show();
                } else {
                    if(_GPSLocation != null) {
                        _location.setLatitude(_GPSLocation.getLatitude());
                        _location.setLongitude(_GPSLocation.getLongitude());
                        _renderer.setLocation(_location);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.gps), Toast.LENGTH_LONG).show();
                        new ImageLoader(_context, _location).executeOnExecutor(_singleThreadExecutor);
                    } else{
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.noLocation), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        _frameLayout.removeView(buttonLocation);
        _frameLayout.addView(buttonLocation);

        LinearLayout license = (LinearLayout) findViewById(R.id.linearLayout);
        _frameLayout.removeView(license);
        _frameLayout.addView(license);

        _isEvent = false;
    }

    private void loadTextureBuffer() {
        float[] _texture = new float[2 * 3 * 2 * (SIDE) * (SIDE)];
        for (int i = 0; i < (SIDE); i++) {
            for (int j = 0; j < (SIDE); j++) {

                _texture[(i*(SIDE) + j) * 12 + 0] = ((float)j/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 1] = (float)(i)/(float)(SIDE);
                _texture[(i*(SIDE) + j) * 12 + 2] = (((j+1f))/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 3] = (float)i/(float)(SIDE);
                _texture[(i*(SIDE) + j) * 12 + 4] = ((float)(j)/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 5] = ((i+1f)/(float)(SIDE));

                _texture[(i*(SIDE) + j) * 12 + 6] = ((float)j/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 7] = ((i+1f)/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 8] = (((float)j+1f)/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 9] = (float)i/(float)(SIDE);
                _texture[(i*(SIDE) + j) * 12 + 10] = (((float)j+1f)/(float)(SIDE));
                _texture[(i*(SIDE) + j) * 12 + 11] = ((float)i+1f)/(float)(SIDE);

            }
        }

        ByteBuffer byteBuf = ByteBuffer.allocateDirect(_texture.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        _textureBuffer = byteBuf.asFloatBuffer();
        _textureBuffer.put(_texture);
        _textureBuffer.position(0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {
                int pointerIndex = event.getActionIndex();
                int pointerID = event.getPointerId(pointerIndex);
                if (pointerID == 0) {
                    _startX = event.getX(pointerIndex);
                    _startY = event.getY(pointerIndex);
                }
                _isEvent = true;
                break;
            }

            case MotionEvent.ACTION_MOVE:
                if(_isEvent) {
                    if (event.getPointerCount() < 2) {
                        Point size = new Point();
                        this.getWindowManager().getDefaultDisplay().getSize(size);
                        int pointerIndex = event.getActionIndex();
                        int pointerID = event.getPointerId(pointerIndex);
                        if (pointerID == 0) {
                            int res = _renderer.setPosuvX((event.getX(pointerIndex) - _startX) / (size.x));
                            this._location = _renderer.getLocation();
                            if(res == 1){
                                new ImageLoader(this, _location).execute();
                                return true;
                            }
                            if(res == -1){

                                new ImageLoader(this, _location).execute();
                                return true;
                            }
                            res = _renderer.setPosuvZ((event.getY(pointerIndex) - _startY) / (size.x));
                            this._location = _renderer.getLocation();
                            if(res == 1) {

                                new ImageLoader(this, _location).execute();
                                return true;
                            }
                            if(res == -1) {

                                new ImageLoader(this, _location).execute();
                                return true;
                            }

                            _startX = event.getX(pointerIndex);
                            _startY = event.getY(pointerIndex);
                        }
                        return true;
                    }
                }
            case MotionEvent.ACTION_POINTER_UP: {
                int pointerIndex = event.getActionIndex();
                int pointerID = event.getPointerId(pointerIndex);
                if (pointerID == 0) {
                    _startX = event.getX(pointerIndex);
                    _startY = event.getY(pointerIndex);
                }
                _isEvent = false;
                break;
            }
        }
        return true;
    }

    private static int[] getTileNumber(final double lat, final double lon, final int zoom) {
        int xtile = (int)Math.floor( (lon + 180) / 360 * (1<<zoom) ) ;
        int ytile = (int)Math.floor( (1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1<<zoom) ) ;
        if (xtile < 0)
            xtile=0;
        if (xtile >= (1<<zoom))
            xtile=((1<<zoom)-1);
        if (ytile < 0)
            ytile=0;
        if (ytile >= (1<<zoom))
            ytile=((1<<zoom)-1);
        return new int[]{xtile,ytile};
    }

    public class ImageLoader extends AsyncTask<Void, Void, Bitmap[]> {

        private Context context;
        private InputStream inputStream;
        private Location loc;
        private static final int ZOOM_12 = 12;

        ImageLoader(Context context,Location _location) {
            loc = _location;
            ImageLoader.this.context = context;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Bitmap[] doInBackground(Void... arg0) {

            for(int i=-1;i<=1;i++) {
                for (int j = -1;j <= 1; j++) {
                    _area[(i + 1) * 3 + (j + 1)] = new Area(context, loc, _textureBuffer, i, j, _noTileBitmap);
                    _area[(i + 1) * 3 + (j + 1)].setBitmap(_noTileBitmap);
                }
            }

            Bitmap[] bitmap = new Bitmap[9];
            for(int i=-1;i<=1;i++) {
                for (int j = -1;j <= 1; j++) {
                    int rids[] = new int[1];
                    int[] tiles = getTileNumber(loc.getLatitude(), loc.getLongitude(), ZOOM_12);
                    tiles[0] += j;
                    tiles[1] += i;
                    try {
                        inputStream = context.getAssets().open("tiles/12/" + tiles[0] + "/" + tiles[1] + ".png");
                        bitmap[(i + 1) * 3 + (j + 1)] = BitmapFactory.decodeStream(inputStream);
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        String imgTile[] = {"no_data"};
                        rids[0] = context.getResources().getIdentifier(imgTile[0], "drawable", context.getPackageName());
                        bitmap[(i + 1) * 3 + (j + 1)] = BitmapFactory.decodeResource(context.getResources(), rids[0]);
                    }
                }
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute( Bitmap[] result )  {
            for(int i=-1;i<=1;i++) {
                for (int j = -1;j <= 1; j++) {
                    _area[(i + 1) * 3 + (j + 1)].setBitmap(result[(i + 1) * 3 + (j + 1)]);
                }
            }
            _renderer.setAreas(_area);
            _renderer.setChange(true);
        }
    }

    public class ImageLoader2 extends AsyncTask<Void, Void, Bitmap> {

        private Context context;

        ImageLoader2(Context context) {
            ImageLoader2.this.context = context;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Bitmap doInBackground(Void... arg0) {
            int rids[] = new int[1];
            String imgTile[] = {"no_data"};
            rids[0] = context.getResources().getIdentifier(imgTile[0], "drawable", context.getPackageName());
            _noTileBitmap = BitmapFactory.decodeResource(context.getResources(), rids[0]);
            return null;
        }

        @Override
        protected void onPostExecute( Bitmap result )  {
            for(int i=-1;i<=1;i++) {
                for (int j = -1; j <= 1; j++) {
                    _area[(i + 1) * 3 + (j + 1)].setBitmap(_noTileBitmap);
                }
            }
        }
    }
}